export class SingleObject {
  name: string;
  private static instance = new SingleObject();
  constructor() {}
  public static getInstance() {
    return SingleObject.instance;
  }
  setName(name: string): void {
    this.name = name;
  }
  consoleName() {
    console.log(this.name);
  }
}
