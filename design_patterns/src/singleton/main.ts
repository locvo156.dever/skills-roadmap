import { SingleObject } from "./single-object";

export function main() {
    const object = SingleObject.getInstance();
    object.setName("Loc");
    console.log(object.name);
    object.setName("Loc edited");
}

main();